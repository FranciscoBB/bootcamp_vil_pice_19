public class TestPerson {

    public static void main(String[] args) {
        Persona pers = new Persona("Juan", "juan@pildorasinformaticas.com", 37, 1);
        Persona pers2 = new Persona("José", "jose@pildorasinformaticas.com", 26, 2);

        PersonaController personaController = new PersonaController();

        personaController.muestraPersona(pers);
        personaController.comparaPersonas(pers, pers2);
    }

}

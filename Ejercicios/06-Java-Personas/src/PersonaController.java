class PersonaController {

    public PersonaController() {

    }

    /**
     * Método que muestra la persona
     * @param pers Persona a mostrar
     */
    public void muestraPersona(Persona pers) {
        System.out.println(pers.getNombre() + " tiene " + pers.getEdad() + " y su email es " + pers.getEmail());
    }

    /**
     * Método que sirve para comparar la edad de dos personas
     * @param pers1 Objeto persona1
     * @param pers2 Objeto persona2
     */
    public void comparaPersonas(Persona pers1, Persona pers2) {
        if(pers1.getEdad() < pers2.getEdad())
            System.out.println(pers1.getNombre() + "(" + pers1.getEdad() + ") " +"es más joven " + "que " + pers2.getNombre() + "(" + pers2.getEdad() + ")");
        else if(pers1.getEdad() > pers2.getEdad())
            System.out.println(pers1.getNombre() + "(" + pers1.getEdad() + ") " +"es mayor " + "que " + pers2.getNombre() + "(" + pers2.getEdad() + ")");
        else
            System.out.println(pers1.getNombre() + "(" + pers1.getEdad() + ") " +"tiene la misma edad " + "que " + pers2.getNombre() + "(" + pers2.getEdad() + ")");
    }
}

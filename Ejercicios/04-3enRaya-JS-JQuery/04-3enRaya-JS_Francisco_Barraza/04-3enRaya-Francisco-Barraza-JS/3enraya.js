var jugadorActual = "O";
var jugador1 = false;
var fatality = false;

function colocar(caja) {
    if (fatality)
        return;
    else if (caja.innerText != ""){
        alert("CASILLA OCUPADA");
        return;
    }
    caja.innerText = jugadorActual;
    if (jugadorActual == "O") {
        jugadorActual = "X";
        jugador1 = false;
    } else {
        jugadorActual = "O";
        jugador1 = true;
    }
    compruebaTablero();
}

function compruebaTablero(){
    var i;
    for(i=0;i<=2;i++){
        //EVALÚA TANTO FILAS
        compruebaGanador(document.getElementById("0_"+i).innerText, document.getElementById("1_"+i).innerText, document.getElementById("2_"+i).innerText);
        //COMO COLUMNAS
        compruebaGanador(document.getElementById(i+"_0").innerText,document.getElementById(i+"_1").innerText,document.getElementById(i+"_2").innerText);
    }
    //LAS DIAGONALES
    compruebaGanador(document.getElementById("0_0").innerText, document.getElementById("1_1").innerText, document.getElementById("2_2").innerText);
    compruebaGanador(document.getElementById("0_2").innerText, document.getElementById("1_1").innerText, document.getElementById("2_0").innerText);
}

function compruebaGanador(primero, segundo, tercero) {
    if (primero != "" && primero == segundo && primero == tercero) {
        var mensaje; //Mensaje de victoria;
        var sonido = new Audio('sounds/gameoveryeah.mp3'); //Cortesía del gran Takenobu Mitsuyoshi, el sonido del GAME OVER más feliz de la historia.
        if (jugador1 == false)
            mensaje = "Player 1 WINS";
        else
            mensaje = "Player 2 WINS";
        document.body.style.backgroundImage = "url(images/gameoveryeah.jpg)"; //Cambia la imagen de fondo.
        sonido.play(); //Música, maestro!
        if(alert(mensaje+"    GAME OVER YEEEAAAHHHHH!!    ")){
            fatality = true; //Indica que hay victoria.    
        }else
            window.location.reload();
        fatality = true; //Indica que hay victoria.
    }
}

function refrescar(boton){
    window.location.reload();
}
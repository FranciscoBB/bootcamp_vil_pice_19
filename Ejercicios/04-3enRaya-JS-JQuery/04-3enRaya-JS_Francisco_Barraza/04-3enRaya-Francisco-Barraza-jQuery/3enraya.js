/* Variables */
var jugadorActual = "O";
var jugador1 = false;
var fatality = false;

//Función que siempre se ejecutará por el onclick, por antonomasia.
function colocar(caja) {
    //Si hay victoria, sale de la función.
    if (fatality)
        return;
        //Si la casilla contiene un valor dentro, avisa, y sale de la función para que no conmuten los valores.
    else if (caja.innerText != "") {
        alert("CASILLA OCUPADA");
        return;
    }
    //Lógica de juego.
    caja.innerText = jugadorActual;
    if (jugadorActual == "O") {
        jugadorActual = "X";
        jugador1 = false;
    } else {
        jugadorActual = "O";
        jugador1 = true;
    }
    compruebaTablero();
}

function compruebaTablero() {
    var i;
    for (i = 0; i <= 2; i++) {
        //EVALÚA TANTO FILAS
        compruebaGanador(($("#0_" + i).text), $("#1_" + i).text, $("#2_" + i).text);
        //COMO COLUMNAS
        compruebaGanador(($(i + "_0").text, $(i + "_1").text, $(i + "_2").text));
    }
    //LAS DIAGONALES
    compruebaGanador($("0_0").text, $("1_1").text, $("2_2").text);
    compruebaGanador($("2_0").text, $("1_1").text, $("0_2").text);
}

function compruebaGanador(primero, segundo, tercero) {
    if (primero != "" && primero == segundo && primero == tercero) {
        var mensaje; //Mensaje de victoria;
        var sonido = new Audio('sounds/gameoveryeah.mp3'); //Cortesía del gran Takenobu Mitsuyoshi, el GAME OVER más feliz de la historia de los videojuegos.
        if (jugador1 == false)
            mensaje = "Player 1 WINS";
        else
            mensaje = "Player 2 WINS";
        document.body.style.backgroundImage = "url(images/gameoveryeah.jpg)"; //Cambia la imagen de fondo.
        sonido.play(); //Música, maestro!
        if (alert(mensaje + "    GAME OVER YEEEAAAHHHHH!!    ")) {
            fatality = true; //Indica que hay victoria.    
        } else
            window.location.reload();
        fatality = true;
    }
}

function refrescar(boton) {
    window.location.reload();
}
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Juegos {

    private static Jugador jugador1, jugador2;
    private static int partidas;
    private static Scanner keyboard = new Scanner(System.in);
    private static boolean ganador;

    public static void pideJugador(int numJugador) {
        String pregunta = String.format("Nombre para el jugador %d: ", numJugador);
        System.out.println(pregunta);
        String nombre = keyboard.next();
        Jugador j = new Jugador("Francis");

        if (numJugador == 1) {
            Juegos.jugador1 = j;
        } else {
            Juegos.jugador2 = j;
        }
    }

    /**
     * Menú principal
     */
    public static void menu() {
        System.out.println("*******************");
        System.out.println("JUEGOS DISPONIBLES:");
        System.out.println("*******************");
        System.out.println("1: Cara o Cruz");
        System.out.println("2: Piedra, Papel o Tijera");
        System.out.println("3: 3 en Raya ");
        System.out.println("*******************");
        //pedimos opcion de juego, comprobando validez
        int opcion = 0;
        do {
            System.out.print("Introduce juego: ");
            opcion = keyboard.nextInt();
            switch (opcion) {
                case 1:
                    Juegos.caraCruz();
                    break;
                case 2:
                    Juegos.piedraPapelTijera();
                    break;
                case 3:
                    Juegos.tresEnRaya();
                    break;
                default:
                    System.exit(0);
                    System.out.println("RUM & COLA... SET IT DOWN, AND WALK AWAY.");
                    break;
            }
        } while (true);
    }

    /**
     * juego caraCruz
     */
    public static void caraCruz() {
        Juegos.pideJugador(1); //solo interviene un jugador en este juego
        Juegos.partidas = 5; //partidas por defecto en este juego
        System.out.println();
        System.out.println("CARA O CRUZ:");
        System.out.println("************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();
        Random rnd = new Random();
//bucle de partidas
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Cara (C) o cruz(X) ? ");
//pedimos apuesta aunque no se utiliza
            String apuesta = keyboard.next();
// sea cual sea la probabilidad es un 50%...
// obtenemos un boolean aleatorio
            boolean ganador = rnd.nextBoolean();
            if (ganador) {
                System.out.println(" Has acertado!");
                Juegos.jugador1.ganadas++;
            } else {
                System.out.println(" Lo siento...");
            }
            Juegos.jugador1.partidas++;
        }
//creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.",

                Juegos.jugador1.nombre,
                Juegos.jugador1.partidas,
                Juegos.jugador1.ganadas);

        System.out.println(resumen);
    }

    public static void piedraPapelTijera() {
        Juegos.pideJugador(1);
        Juegos.partidas = 5;
        System.out.println();
        System.out.println("PIEDRA, PAPEL, TIJERA:");
        System.out.println("**********************");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        //bucle de partidas
        for (int i = 0; i < Juegos.partidas; i++) {
            System.out.print("Piedra(O), Papel(|), o Tijera(X) ? ");

            String apuesta = keyboard.next();
            ArrayList<String> combinaciones = new ArrayList<>();
            combinaciones.add("O");
            combinaciones.add("X");
            combinaciones.add("|");
            Random rnd = new Random();
            int resultado = rnd.nextInt(combinaciones.size() - 1);

            switch (apuesta) {
                case "O":
                    if (resultado == 0) {
                        ganador = true;
                        jugador1.ganadas++;
                    } else if (combinaciones.get(0).equals(apuesta)) {
                        ganador = false;
                        jugador1.empates++;
                    } else {
                        ganador = false;
                    }
                    break;
                case "|":
                    if (resultado == 1) {
                        jugador1.ganadas++;
                        ganador = true;
                    } else if (combinaciones.get(1).equals(apuesta)) {
                        ganador = false;
                        jugador1.empates++;
                    } else {
                        ganador = false;
                    }
                    break;
                case "X":
                    if (resultado == 2) {
                        ganador = true;
                        jugador1.ganadas++;
                    } else if (combinaciones.get(2).equals(apuesta)) {
                        ganador = false;
                        jugador1.empates++;
                    } else {
                        ganador = false;
                    }
                    break;
                default:
                    System.out.println("OCELOT IS DEAD -- @#!*ñ+ÇW¿ÇTQL·$*FKQ -- TIME PARADOX");
                    break;
            }
            Juegos.jugador1.partidas++;
        }
//creamos string con el resumen final de juego y lo mostramos
        String resumen = String.format("Jugador %s, %d partidas, %d ganadas.",

                Juegos.jugador1.nombre,
                Juegos.jugador1.partidas,
                Juegos.jugador1.ganadas);

        System.out.println(resumen);
    }

    public static void tresEnRaya() {
        Juegos.pideJugador(1);
        Juegos.partidas = 5;
        System.out.println();
        System.out.println("3 en RAYA");
        System.out.println("**********");
        System.out.printf("Bienvenido %s!", Juegos.jugador1.nombre);
        System.out.println();
        System.out.printf("Vamos a jugar %d partidas", Juegos.partidas);
        System.out.println();

        char simbolo = 'X';

        char[][] posiciones = new char[3][3];
        Juegos.partidas = 0;

        //bucle de partidas
        while (Juegos.partidas <= 4) {

            for (int tirada = 1; tirada <= 9; tirada++) {

                System.out.println("Seleccione fila");
                int fila = keyboard.nextInt();
                System.out.println("Seleccione columna");
                int columna = keyboard.nextInt();

                if (simbolo == 'X')
                    simbolo = 'O';
                else
                    simbolo = 'X';

                if (posiciones[fila][columna] == '\0')
                    posiciones[fila][columna] = simbolo;

                for (int a = 0; a <= 2; a++) {
                    System.out.println(" | " + posiciones[a][a] + " | " + posiciones[a][a] + " | " + posiciones[a][a]);
                }
                System.out.println("");
            }

            for (int i = 0; i <= 2; i++) {
                if (posiciones[0][i] == simbolo && posiciones[1][i] == simbolo && posiciones[2][i] == simbolo)
                    ganador = true;
                else if (posiciones[i][0] == simbolo && posiciones[i][1] == simbolo && posiciones[i][2] == simbolo)
                    ganador = true;
            }
            if ((posiciones[0][2] == simbolo && posiciones[1][1] == simbolo && posiciones[2][0] == simbolo) && ((posiciones[2][0] == simbolo && posiciones[1][1] == simbolo && posiciones[0][2] == simbolo))) {
                ganador = true;
            }
            System.out.println("\nHA GANADO EL JUGADOR CON EL SÍMBOLO " + simbolo);
            Juegos.partidas++;
        }
    }

    public static void ruleta() {

    }

}
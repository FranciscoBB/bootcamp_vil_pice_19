public class Jugador {

    String nombre;
    int empates, ganadas, partidas;

    public Jugador(String nombre) {
        this.nombre = nombre;
        this.empates = 0;
        this.ganadas = 0;
        this.partidas = 0;
    }
}

public class Conversor {

    private double valor1, valor2;
    private String unidad1, unidad2;

    public Conversor() {

    }

    public double convierte(double valor1, String unidad1, String unidad2) {
        setValor1(valor1);
        setUnidad1(unidad1);
        setUnidad2(unidad2);

        switch (getUnidad1()) {
            case "km/h":
                switch (getUnidad2()) {
                    case "mph":
                        valor2 = valor1 * 0.621371192237334;
                        break;
                    case "yardas/s":
                        valor2 = valor1 * 0.303781471760474;
                        break;
                    case "m/s":
                        valor2 = valor1 * 1000 / 3600;
                        break;
                }
                break;
            case "mph":
                switch (getUnidad2()) {
                    case "km/h":
                        valor2 = valor1 * 1.609344;
                        break;
                    case "yardas/s":
                        valor2 = valor1 * 1760 / 3600;
                        break;
                    case "m/s":
                        valor2 = valor1 * 0.44704;
                        break;
                }
                break;
            case "m/s":
                switch (getUnidad2()) {
                    case "km/h":
                        valor2 = valor1 * 1000 / 3600;
                        break;
                    case "yardas/s":
                        valor2 = valor1 * 1.09361329833771;
                        break;
                    case "mph":
                        valor2 = valor1 * 2.2369362920544;
                        break;
                }
                break;
            case "yarda/s":
                switch (getUnidad2()) {
                    case "km/h":
                        valor2 = valor1 * 3.29184;
                        break;
                    case "mph":
                        valor2 = valor1 * 1760 / 3600;
                        break;
                    case "m/s":
                        valor2 = valor1 * 0.0005681818181818;
                        break;
                }
                break;
        }
        return valor2;
    }


    public double getValor1() {
        return valor1;
    }

    public void setValor1(double valor1) {
        this.valor1 = valor1;
    }

    public String getUnidad1() {
        return unidad1;
    }

    public void setUnidad1(String unidad1) {
        this.unidad1 = unidad1;
    }

    public String getUnidad2() {
        return unidad2;
    }

    public void setUnidad2(String unidad2){
        this.unidad2 = unidad2;
    }

    public double getValor2(){
        return convierte(getValor1(), getUnidad1(), getUnidad2());
    }
}

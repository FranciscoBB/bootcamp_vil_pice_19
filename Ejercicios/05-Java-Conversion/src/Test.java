import java.util.Scanner;

public class Test {

    public static void main(String[] args) {
        Conversor cnv = new Conversor();
        Scanner sc = new Scanner(System.in);
        System.out.println("Qué quiere convertir?");
        double valor1 = sc.nextDouble();
        System.out.println("En qué unidad de velocidad?\n");
        String unidad1, unidad2;
        unidad1 = sc.nextLine();
        System.out.println("A qué velocidad quiere convertirla?");
        unidad2 = sc.nextLine();

        cnv.setValor1(valor1);
        cnv.setUnidad1(unidad1);
        cnv.setUnidad2(unidad2);


        System.out.printf("%.2f %s equivalen a %.2f %s", cnv.getValor1(), cnv.getUnidad1(), cnv.getValor2(), cnv.getUnidad2());
        sc.close();
    }
}